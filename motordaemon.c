/**
 * @file motordaemon.c 
 *
 */

/* standard robotic includes */
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h> // for atoi
#include <robotcontrol.h> // includes ALL Robot Control subsystems

/* serial communication includes */
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

#define I2C_BUS 2
#define INIT_SPEED 0.6

/**
 * This template contains these critical components
 * - ensure no existing instances are running and make new PID file
 * - start the signal handler
 * - initialize subsystems motor and magnetometer 
 * - while loop that checks for EXITING condition
 * - cleanup subsystems at the end
 *
 * @return     0 during normal operation, -1 on error
 */

/* used variables */
rc_mpu_data_t sensordata;	//struct to hold new data
struct robotdata{       // struct to hold robot state
  char botname[50];
  int front_distance;
  double x_orient;	// holds compass orientation value, x axis
  double y_orient;	// holds compass orientation value, y axis
  double motorspeed_left;	// speed for motor 1
  double motorspeed_right;	// speed for motor 2
  double motorspeed;	// general motor speed
  double servopos_head;	// servo position head
  double servopos_left;	// servo position left arm
  double servopos_right;	// servo position right arm
  int is_turning;
} mybot;	

/* nice aliases for motor and servo numbering */
int motor_left = 1;
int motor_right = 2;
int servo_head = 1;
int servo_left = 2;
int servo_right = 3;


/* serial communication to arduino */
const char *device = "/dev/ttyUSB0";
int tty_fd;
char ch='D';

double angle = 100;

/* function declarations */
int init_board();
int parse_arguments(int argc, char *argv[]);
int read_sensors(rc_mpu_data_t sensor, struct robotdata *bot); 
int update_mybot(struct robotdata *bot);
int move_motors(struct robotdata *bot);
int move_servos(struct robotdata *bot);

static void __print_usage(void){
	printf("\n");
	printf(" Options\n");
	printf(" -a {angle}   Specify angle between 0 - 100\n");
	printf(" -h           Print this help messege \n\n");
}

int parse_arguments(int argc, char *argv[]){
	// parse arguments
	opterr = 0;
	int c;
	while ((c = getopt(argc, argv, "a:h")) != -1){
		switch (c){
		// get angle 
		case 'a':
			angle = atof(optarg);
			printf("\nGiven Argument for angle: %.f.\n", angle);
			break;
		// help mode
		case 'h':
			__print_usage();
			return 0;

		default:
			printf("\nInvalid Argument \n");
			__print_usage();
			return -1;
		}
	}
	if (angle < 1 || angle > 100) {
		printf("\nPlease provide \"-a\" with a valid angle.\n");
		__print_usage();
		return -1;
	} 
	else {
		printf("\nGiven Argument for angle: %.f.\n", angle);
	}
	return 0;

}

int init_board(){
	if(rc_kill_existing_process(2.0)<-2) return -1;
	rc_make_pid_file();
	if(rc_enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal handler\n");
		return -1;
	}
	// use defaults for now, except also enable magnetometer.
	rc_mpu_config_t conf = rc_mpu_default_config();
	conf.i2c_bus = I2C_BUS;
	conf.enable_magnetometer = 1;
	conf.show_warnings = 1;

	if(rc_mpu_initialize(&sensordata, conf)){
		fprintf(stderr,"rc_mpu_initialize_failed\n");
		return -1;
	}
	// read adc to make sure battery is connected
	if(rc_adc_init()){
		fprintf(stderr,"ERROR: failed to run rc_adc_init()\n");
		fprintf(stderr,"Is a battery connected?\n");
		return -1;
	}
	if(rc_adc_batt()<6.0){
		fprintf(stderr,"ERROR: battery disconnected or insufficiently charged to drive servos\n");
		return -1;
	}
	printf("Battery level: %f\n", rc_adc_batt());
	rc_adc_cleanup();

	struct termios tio;
        tio.c_iflag=0;
        tio.c_oflag=0;
        tio.c_cflag=CS8|CREAD|CLOCAL;           // 8n1, see termios.h for more information
        tio.c_lflag=0;
        cfsetospeed(&tio,B115200);            // 115200 baud
        cfsetispeed(&tio,B115200);            // 115200 baud
        tty_fd=open(device, O_RDWR | O_NONBLOCK); 
	printf("tty file descriptor value: %i\n", tty_fd);
	printf("tty input speed value: %i\n", cfgetispeed(&tio));
        tcsetattr(tty_fd,TCSANOW,&tio);

	
	// initialize motors
	int freq_hz = RC_MOTOR_DEFAULT_PWM_FREQ;
	if (rc_motor_init_freq(freq_hz)) { 
		fprintf(stderr,"ERROR: failed to initialize motors\n");
		return -1;
	}
	rc_motor_free_spin(1);
	rc_motor_free_spin(2);
	// initialize PRU
	if(rc_servo_init()) return -1;

	// turn on power
	printf("Turning On 6V Servo Power Rail\n");
	rc_servo_power_rail_en(1);

	// print out what the program is doing
	printf("\n");
	printf("Starting to control robot head.\n");

	printf("\nHello BeagleBone\n");
	printf("\nTwo motors are now initialized.\n");
	// Keep looping until state changes to EXITING
	rc_set_state(RUNNING);
	return 0;
}

int read_sensors(rc_mpu_data_t sensor, struct robotdata *bot){
	// read magnetometer
	if(rc_mpu_read_mag(&sensor)){
		printf("read mag data failed\n");
	}
	bot->x_orient = sensor.mag[0];
	bot->y_orient = sensor.mag[1];
	// get terminal output, read front sensor
	//printf("Debug: tty file descriptor value: %i\n", tty_fd);
	//printf("Debug: tty input speed value: %i\n", cfgetispeed(&tio));
	//printf("Debug: serial char: %s\n", &ch);
	//printf("Debug: serial int: %i\n", atoi(&ch));
	//printf("Debug: read function return %i\n", read(tty_fd,&ch,5));
        if (read(tty_fd,&ch,5)>0) {
		bot->front_distance = atoi(&ch);
	}
	else {
	//	printf("Debug: tty %i not ready, no data...\n", tty_fd);
	}
	printf("Debug: mag sensor x: %f\n", bot->x_orient);
	printf("Debug: mag sensor y: %f\n", bot->y_orient);
	printf("Debug: front distance sensor: %i\n", bot->front_distance);
	return 0;
}

int move_motors(struct robotdata *bot){
	if(rc_motor_set(motor_left, bot->motorspeed_left)==-1){
		printf("Motor %i did not accept signal\n", motor_left);
		return -1;
	}
	if(rc_motor_set(motor_right, bot->motorspeed_right)==-1){
		printf("Motor %i did not accept signal\n", motor_right);
		return -1;
	}
	return 0;
}

int move_servos(struct robotdata *bot){
	if(rc_servo_send_pulse_normalized(servo_head, bot->servopos_head)==-1){ 
		printf("Servo %i did not accept signal\n", servo_head);
		return -1;
	}
	if(rc_servo_send_pulse_normalized(servo_left, bot->servopos_left)==-1){ 
		printf("Servo %i did not accept signal\n", servo_left);
		return -1;
	}
	if(rc_servo_send_pulse_normalized(servo_right, bot->servopos_right)==-1){ 
		printf("Servo %i did not accept signal\n", servo_right);
		return -1;
	}
	//printf("tried to set servo %i to position %f\n", servo_right, bot->servopos_right);
	return 0;
}

int update_mybot(struct robotdata *bot){
	bot->servopos_head = 0.0;
	bot->servopos_left = 1.0/100*bot->front_distance;
	bot->servopos_right = 1.0/100*-1*bot->front_distance;
	printf("Debug: left servoposition: %f\n", bot->servopos_left);
	printf("Debug: right servoposition: %f\n", bot->servopos_right);
	if (bot->is_turning == 0){ 			// we are going straight
		printf("Debug: turn value: %i\n", bot->is_turning);
		if (bot->front_distance > 60){
			bot->motorspeed = 1.0;
		} else {
			bot->motorspeed = 1.0/100*bot->front_distance;
		}

		if (bot->front_distance > 30){
			bot->motorspeed_right = bot->motorspeed;
			bot->motorspeed_left = -bot->motorspeed;
		}
		if (bot->front_distance < 30){
			bot->is_turning = 1;		// we need to turn
		}
	} else {	
		printf("Debug: turn value: %i\n", bot->is_turning);
		//quadrant 1
		if(bot->x_orient > 0 && bot->y_orient < 0){
			// turn clockwise towards west
			if(bot->y_orient < -1){
				bot->motorspeed_right = -0.4;
				bot->motorspeed_left = -0.4;
				bot->is_turning = 1;
			}
			else {
				bot->is_turning = 0;
			}
		}
		//quadrant 2
		if(bot->x_orient < 0 && bot->y_orient < 0){
			// turn counter clockwise towards north
			if(bot->y_orient > -24){
				bot->motorspeed_right = 0.4;
				bot->motorspeed_left = 0.4;
				bot->is_turning = 1;
			}
			else {
				bot->is_turning = 0;
			}
		}
		//quadrant 3
		if(bot->x_orient < 0 && bot->y_orient > 0){
			// turn counter clockwise towards east
			if(bot->y_orient > 1){
				bot->motorspeed_right = 0.4;
				bot->motorspeed_left = 0.4;
				bot->is_turning = 1;
			}
			else {
				bot->is_turning = 0;
			}
		}
		//quadrant 4
		if(bot->x_orient > 0 && bot->y_orient > 0){
			// turn clockwise towards south
			if(bot->y_orient < 24){
				bot->motorspeed_right = -0.4;
				bot->motorspeed_left = -0.4;
				bot->is_turning = 1;
			}
			else {
				bot->is_turning = 0;
			}

		}
	}
	return 0;
}

int main(int argc, char *argv[]){
	if(parse_arguments(argc, argv)==-1){
		return -1;
	}
	if(init_board()==-1){
		printf("Could not initialize board\n");
		return -1;
	}
	/* terminal intialization stuff */
	while(rc_get_state()!=EXITING){
		read_sensors(sensordata, &mybot);
		update_mybot(&mybot);
		move_motors(&mybot);
		move_servos(&mybot);
		// always sleep at some point
		rc_usleep(100000);
	}
	rc_mpu_power_off();
	printf("\nCalling rc_motor_cleanup()\n");
	rc_motor_cleanup();
	// turn off power rail and cleanup
	printf("\nTurning Off 6V Servo Power Rail\n");
	rc_servo_power_rail_en(0);
	printf("\nCalling rc_servo_cleanup()\n");
	rc_servo_cleanup();
	rc_remove_pid_file();	// remove pid file LAST
	return 0;
}


